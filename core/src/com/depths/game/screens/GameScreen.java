/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.screens;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.depths.game.Depths;
import com.depths.game.controllers.KeyboardController;
import com.depths.game.ecs.components.PlayerComponent;
import com.depths.game.ecs.systems.AnimationSystem;
import com.depths.game.ecs.systems.BulletSystem;
import com.depths.game.ecs.systems.CollisionSystem;
import com.depths.game.ecs.systems.EnemySystem;
import com.depths.game.ecs.systems.LevelGenerationSystem;
import com.depths.game.ecs.systems.LightSystem;
import com.depths.game.ecs.systems.ParticleEffectSystem;
import com.depths.game.ecs.systems.PhysicsDebugSystem;
import com.depths.game.ecs.systems.PhysicsSystem;
import com.depths.game.ecs.systems.PlayerControlSystem;
import com.depths.game.ecs.systems.RenderingSystem;
import com.depths.game.ecs.systems.SteeringSystem;
import com.depths.game.loader.DepthsAssetManager;
import com.depths.game.physics.factory.LevelFactory;
import com.depths.game.util.FrameRate;

public class GameScreen implements Screen {

    private final Depths parent; // a field to store our orchestrator
    private final OrthographicCamera cam;
    private final KeyboardController controller;
    private final SpriteBatch sb;
    private final PooledEngine engine;
    private final DepthsAssetManager atlas;
    private final LevelFactory lvlFactory;
    private Entity player;
    private final FrameRate fps;

    // our constructor with a depths argument
    public GameScreen(Depths depths) {
        parent = depths; // setting the argument to our field.

        // gets the images as a texture
        atlas = parent.assetManager;

        controller = new KeyboardController();

        // create a pooled engine
        engine = new PooledEngine();
        lvlFactory = new LevelFactory(engine, atlas);

        sb = new SpriteBatch();

        // Create our new rendering system
        final RenderingSystem renderingSystem = new RenderingSystem(sb);
        cam = renderingSystem.getCamera();
        final ParticleEffectSystem particleSystem = new ParticleEffectSystem(sb,
                cam);
        sb.setProjectionMatrix(cam.combined);

        // add all the relevant systems our engine should run
        engine.addSystem(new AnimationSystem());
        engine.addSystem(new PhysicsSystem(lvlFactory.world));
        engine.addSystem(new PlayerControlSystem(controller, lvlFactory));
        engine.addSystem(renderingSystem); // not a fan of splitting batch into
                                           // rendering and
                                           // particles but I like the
                                           // separation of the systems
        engine.addSystem(particleSystem); // particle get drawn on top so should
                                          // be placed after
                                          // normal rendering
        engine.addSystem(new PhysicsDebugSystem(lvlFactory.world,
                renderingSystem.getCamera(), parent));
        engine.addSystem(new CollisionSystem());
        player = lvlFactory.createPlayer(cam);
        engine.addSystem(new EnemySystem(lvlFactory));
        engine.addSystem(new BulletSystem(lvlFactory));
        engine.addSystem(new LevelGenerationSystem(lvlFactory));
        engine.addSystem(new SteeringSystem());
        engine.addSystem(new LightSystem());

        lvlFactory.createFloor();
        fps = new FrameRate();

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(controller);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        engine.update(delta);

        // check if player is dead. if so show end screen
        final PlayerComponent pc = player.getComponent(PlayerComponent.class);
        if (pc.isDead) {
            Gdx.app.debug(this.getClass().getSimpleName(),
                    "YOU DIED : back to menu you go!");
            parent.lastScore = (int) pc.cam.position.y;
            parent.changeScreen(Screens.ENDGAME);
        }

        lvlFactory.rayHandler.setCombinedMatrix(cam);
        lvlFactory.rayHandler.update();
        lvlFactory.rayHandler.render();

        fps.update();
        fps.render();

    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
// Don't think we want to do this since we use them for the next game.
//        rayHandler.dispose();
//        lvlFactory.world.dispose();
    }

    public void resetWorld() {

        Gdx.app.debug(this.getClass().getSimpleName(), "Resetting world");
        engine.removeAllEntities();

        lvlFactory.resetWorld();
        player = lvlFactory.createPlayer(cam);
        lvlFactory.createFloor();

        // reset controller controls (fixes bug where controller stuck on
        // directrion if died in that
        // position)
        controller.left = false;
        controller.right = false;
        controller.up = false;
        controller.down = false;
        controller.isMouse1Down = false;
        controller.isMouse2Down = false;
        controller.isMouse3Down = false;

    }

}
