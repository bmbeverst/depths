/*
 * MIT License
 *
 * Copyright (c) 2022 Brooks B
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.loader;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader.ParticleEffectParameter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class DepthsAssetManager {

    public final AssetManager manager = new AssetManager();

    // Textures
    public final String gameImages = "images/depthsGame.atlas";
    public final String loadingImages = "images/depthsLoading.atlas";

    public void queueAddImages() {
        manager.load(gameImages, TextureAtlas.class);
    }

    // a small set of images used by the loading screen
    public void queueAddLoadingImages() {
        manager.load(loadingImages, TextureAtlas.class);
    }

    // Sounds
    public final String boingSound = "sounds/boing.wav";
    public final String pingSound = "sounds/ping.wav";

    public void queueAddSounds() {
        manager.load(boingSound, Sound.class);
        manager.load(pingSound, Sound.class);
    }

    // Music
    public final String playingSong = "music/Rolemusic_-_pl4y1ng.mp3";

    public void queueAddMusic() {
        manager.load(playingSong, Music.class);
    }

    // Skin
    public final String skin = "skin/uiskin.json";

    public void queueAddSkin() {
        manager.load(skin, Skin.class);
    }

    public void queueAddFonts() {
        // TODO Auto-generated method stub

    }

    // Particle Effects
    public final String smokeEffect = "particles/smoke.pe";
    public final String waterEffect = "particles/water.pe";
    public final String fireEffect = "particles/fire.pe";
    public final String blueFireEffect = "particles/blueFire.pe";
    public final String lightBeamEffect = "particles/lightBeam.pe";

    public void queueAddParticleEffects() {
        final ParticleEffectParameter pep = new ParticleEffectParameter();
        pep.atlasFile = "images/depthsGame.atlas";
        manager.load(smokeEffect, ParticleEffect.class, pep);
        manager.load(waterEffect, ParticleEffect.class, pep);
        manager.load(fireEffect, ParticleEffect.class, pep);
        manager.load(blueFireEffect, ParticleEffect.class, pep);
        manager.load(lightBeamEffect, ParticleEffect.class, pep);

    }

}
