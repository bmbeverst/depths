/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ai;

import com.badlogic.gdx.ai.steer.behaviors.Arrive;
import com.badlogic.gdx.ai.steer.behaviors.Flee;
import com.badlogic.gdx.ai.steer.behaviors.Seek;
import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.depths.game.ecs.components.SteeringComponent;

public class SteeringPresets {

    public static Wander<Vector2> getWander(SteeringComponent scom) {
        validate(scom);
        // let wander behavior manage facing
        return new Wander<>(scom).setFaceEnabled(false)
                .setWanderOffset(20f) // distance away
                                      // from entity
                                      // to set target
                .setWanderOrientation(0f) // the initial orientation
                .setWanderRadius(10f) // size of target
                .setWanderRate(MathUtils.PI2 * 2); // higher values = more
                                                   // spinning
    }

    public static Seek<Vector2> getSeek(SteeringComponent seeker,
            SteeringComponent target) {
        validate(seeker, target);
        return new Seek<>(seeker, target);
    }

    public static Flee<Vector2> getFlee(SteeringComponent runner,
            SteeringComponent fleeingFrom) {
        validate(runner, fleeingFrom);
        return new Flee<>(runner, fleeingFrom);
    }

    public static Arrive<Vector2> getArrive(SteeringComponent runner,
            SteeringComponent target) {
        validate(runner, target);
        // default 0.1f
        return new Arrive<>(runner, target).setTimeToTarget(0.1f)
                .setArrivalTolerance(7f).setDecelerationRadius(10f);
    }

    private static void validate(SteeringComponent target) {
        if (target == null) {
            throw new IllegalArgumentException(
                    "Args must be non-null for SteeringPresets");
        }
    }

    private static void validate(SteeringComponent runner,
            SteeringComponent target) {
        if (runner == null || target == null) {
            throw new IllegalArgumentException(
                    "Both args must be non-null for SteeringPresets");
        }
    }
}