/*
 * MIT License
 *
 * Copyright (c) 2022 Brooks B
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.util;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.utils.IntMap;

/**
 * A pooled particle effect manager to store particle effect pools
 *
 */
public class ParticleEffectManager {

    // DEFINE constants for particleEffects
    public enum Type {
        SMOKE, WATER, FIRE, BLUEFIRE, LIGHTBEAM
    }

    // create intmaps for effects and pools
    private final IntMap<ParticleEffect> partyEffects;
    private final IntMap<ParticleEffectPool> partyEffectPool;

    /**
     * Particle Effect Manager for controlling creating pools and dispensing
     * particle effects
     */
    public ParticleEffectManager() {
        partyEffects = new IntMap<>();
        partyEffectPool = new IntMap<>();
    }

    /**
     * Create a particle effect pool for type with default values (scale 1, pool
     * init capacity 5, max capacity 20)
     *
     * @param type  int id of particle effect
     * @param party the particle effect
     */
    public void addParticleEffect(Type type, ParticleEffect party) {
        addParticleEffect(type, party, 1);
    }

    /**
     * Create a particle effect pool for type with scale and default pool sizes
     *
     * @param type  int id of particle effect
     * @param party the particle effect
     * @param scale The particle effect scale
     */
    public void addParticleEffect(Type type, ParticleEffect party,
            float scale) {
        addParticleEffect(type, party, scale, 5, 20);

    }

    /**
     * Create a particle effect pool for type
     *
     * @param type          int id of particle effect
     * @param party         the particle effect
     * @param scale         The particle effect scale
     * @param startCapacity pool initial capacity
     * @param maxCapacity   pool max capacity
     */
    public void addParticleEffect(Type type, ParticleEffect party, float scale,
            int startCapacity, int maxCapacity) {
        party.scaleEffect(scale);
        partyEffects.put(type.ordinal(), party);
        partyEffectPool.put(type.ordinal(),
                new ParticleEffectPool(party, startCapacity, maxCapacity));

    }

    /**
     * Get a particle effect of type type
     *
     * @param  type the type to get
     * @return      The pooled particle effect
     */
    public PooledEffect getPooledParticleEffect(Type type) {
        return partyEffectPool.get(type.ordinal()).obtain();
    }
}