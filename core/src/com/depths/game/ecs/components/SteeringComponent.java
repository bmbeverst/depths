/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.utils.Location;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.depths.game.physics.Box2dLocation;
import com.depths.game.util.DFUtils;

public class SteeringComponent
        implements Steerable<Vector2>, Component, Poolable {

    public static enum SteeringState {
        WANDER, SEEK, FLEE, ARRIVE, NONE
    } // a list of possible behaviors

    public SteeringState currentMode = SteeringState.WANDER; // stores which
                                                             // state the entity
                                                             // is
                                                             // currently in
    public Body body; // stores a reference to our Box2D body

    // Steering data
    float maxLinearSpeed = 2f; // stores the max speed the entity can go
    float maxLinearAcceleration = 5f; // stores the max acceleration
    float maxAngularSpeed = 50f; // the max turning speed
    float maxAngularAcceleration = 5f;// the max turning acceleration
    float zeroThreshold = 0.1f; // how accurate should checks be (0.0000001f
                                // will mean the entity
                                // must get within 0.0000001f of
    // target location. This will cause problems as our entities travel pretty
    // fast and can easily
    // over or under shoot this.)
    public SteeringBehavior<Vector2> steeringBehavior; // Stores the action
                                                       // behavior
    private static final SteeringAcceleration<Vector2> steeringOutput = new SteeringAcceleration<>(
            new Vector2()); // this is the actual steering vector for our unit
    private final float boundingRadius = 1f; // the minimum radius size for a
                                             // circle required to
                                             // cover whole object
    private boolean tagged = true; // This is a generic flag utilized in a
                                   // variety of ways. (never
                                   // used this myself)
    private boolean independentFacing = false; // defines if the entity can move
                                               // in a direction
                                               // other than the way it faces)

    @Override
    public void reset() {
        currentMode = SteeringState.NONE;
        body = null;
        steeringBehavior = null;

    }

    public boolean isIndependentFacing() {
        return independentFacing;
    }

    public void setIndependentFacing(boolean independentFacing) {
        this.independentFacing = independentFacing;
    }

    /**
     * Call this to update the steering behavior (per frame)
     *
     * @param delta delta time between frames
     */
    public void update(float delta) {
        if (steeringBehavior != null) {
            steeringBehavior.calculateSteering(steeringOutput);
            applySteering(steeringOutput, delta);
        }
    }

    /**
     * apply steering to the Box2d body
     *
     * @param steering  the steering vector
     * @param deltaTime the delta time
     */
    protected void applySteering(SteeringAcceleration<Vector2> steering,
            float deltaTime) {
        boolean anyAccelerations = false;

        // Update position and linear velocity.
        if (!steeringOutput.linear.isZero()) {
            // this method internally scales the force by deltaTime
            body.applyForceToCenter(steeringOutput.linear, true);
            anyAccelerations = true;
        }

        // Update orientation and angular velocity
        if (isIndependentFacing()) {
            if (steeringOutput.angular != 0) {
                // this method internally scales the torque by deltaTime
                body.applyTorque(steeringOutput.angular, true);
                anyAccelerations = true;
            }
        } else {
            // If we haven't got any velocity, then we can do nothing.
            final Vector2 linVel = getLinearVelocity();
            if (!linVel.isZero(getZeroLinearSpeedThreshold())) {
                final float newOrientation = vectorToAngle(linVel);
                // this is superfluous if independentFacing is always true
                body.setAngularVelocity(
                        (newOrientation - getAngularVelocity()) * deltaTime);
                body.setTransform(body.getPosition(), newOrientation);
            }
        }

        if (anyAccelerations) {
            // Cap the linear speed
            final Vector2 velocity = body.getLinearVelocity();
            final float currentSpeedSquare = velocity.len2();
            final float maxLinearSpeed = getMaxLinearSpeed();
            if (currentSpeedSquare > (maxLinearSpeed * maxLinearSpeed)) {
                body.setLinearVelocity(velocity.scl(maxLinearSpeed
                        / (float) Math.sqrt(currentSpeedSquare)));
            }
            // Cap the angular speed
            final float maxAngVelocity = getMaxAngularSpeed();
            if (body.getAngularVelocity() > maxAngVelocity) {
                body.setAngularVelocity(maxAngVelocity);
            }
        }
    }

    @Override
    public Vector2 getPosition() {
        return body.getPosition();
    }

    @Override
    public float getOrientation() {
        return body.getAngle();
    }

    @Override
    public void setOrientation(float orientation) {
        body.setTransform(getPosition(), orientation);
    }

    @Override
    public float vectorToAngle(Vector2 vector) {
        return DFUtils.vectorToAngle(vector);
    }

    @Override
    public Vector2 angleToVector(Vector2 outVector, float angle) {
        return DFUtils.angleToVector(outVector, angle);
    }

    @Override
    public Location<Vector2> newLocation() {
        return new Box2dLocation();
    }

    @Override
    public float getZeroLinearSpeedThreshold() {
        return zeroThreshold;
    }

    @Override
    public void setZeroLinearSpeedThreshold(float value) {
        zeroThreshold = value;
    }

    @Override
    public float getMaxLinearSpeed() {
        return maxLinearSpeed;
    }

    @Override
    public void setMaxLinearSpeed(float maxLinearSpeed) {
        this.maxLinearSpeed = maxLinearSpeed;
    }

    @Override
    public float getMaxLinearAcceleration() {
        return maxLinearAcceleration;
    }

    @Override
    public void setMaxLinearAcceleration(float maxLinearAcceleration) {
        this.maxLinearAcceleration = maxLinearAcceleration;
    }

    @Override
    public float getMaxAngularSpeed() {
        return maxAngularSpeed;
    }

    @Override
    public void setMaxAngularSpeed(float maxAngularSpeed) {
        this.maxAngularSpeed = maxAngularSpeed;
    }

    @Override
    public float getMaxAngularAcceleration() {
        return maxAngularAcceleration;
    }

    @Override
    public void setMaxAngularAcceleration(float maxAngularAcceleration) {
        this.maxAngularAcceleration = maxAngularAcceleration;
    }

    @Override
    public Vector2 getLinearVelocity() {
        return body.getLinearVelocity();
    }

    @Override
    public float getAngularVelocity() {
        return body.getAngularVelocity();
    }

    @Override
    public float getBoundingRadius() {
        return boundingRadius;
    }

    @Override
    public boolean isTagged() {
        return tagged;
    }

    @Override
    public void setTagged(boolean tagged) {
        this.tagged = tagged;
    }
}