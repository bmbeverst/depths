/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.components;

import com.badlogic.ashley.core.ComponentMapper;

public class Mapper {
    public static final ComponentMapper<AnimationComponent> animCom = ComponentMapper
            .getFor(AnimationComponent.class);
    public static final ComponentMapper<B2dBodyComponent> b2dCom = ComponentMapper
            .getFor(B2dBodyComponent.class);
    public static final ComponentMapper<BulletComponent> bulletCom = ComponentMapper
            .getFor(BulletComponent.class);
    public static final ComponentMapper<CollisionComponent> collisionCom = ComponentMapper
            .getFor(CollisionComponent.class);
    public static final ComponentMapper<EnemyComponent> enemyCom = ComponentMapper
            .getFor(EnemyComponent.class);
    public static final ComponentMapper<PlayerComponent> playerCom = ComponentMapper
            .getFor(PlayerComponent.class);
    public static final ComponentMapper<StateComponent> stateCom = ComponentMapper
            .getFor(StateComponent.class);
    public static final ComponentMapper<TextureComponent> texCom = ComponentMapper
            .getFor(TextureComponent.class);
    public static final ComponentMapper<TransformComponent> transCom = ComponentMapper
            .getFor(TransformComponent.class);
    public static final ComponentMapper<TypeComponent> typeCom = ComponentMapper
            .getFor(TypeComponent.class);
    public static final ComponentMapper<SteeringComponent> steerCom = ComponentMapper
            .getFor(SteeringComponent.class);
    public static final ComponentMapper<ParticleEffectComponent> peCom = ComponentMapper
            .getFor(ParticleEffectComponent.class);
    public static final ComponentMapper<LightComponent> lightCom = ComponentMapper
            .getFor(LightComponent.class);
}