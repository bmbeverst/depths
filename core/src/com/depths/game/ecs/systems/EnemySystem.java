/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.depths.game.ai.SteeringPresets;
import com.depths.game.ecs.components.B2dBodyComponent;
import com.depths.game.ecs.components.BulletComponent;
import com.depths.game.ecs.components.EnemyComponent;
import com.depths.game.ecs.components.EnemyComponent.Type;
import com.depths.game.ecs.components.Mapper;
import com.depths.game.ecs.components.SteeringComponent;
import com.depths.game.physics.factory.LevelFactory;
import com.depths.game.util.DFUtils;

public class EnemySystem extends IteratingSystem {

    private final ComponentMapper<EnemyComponent> em;
    private final ComponentMapper<B2dBodyComponent> bodm;
    private final LevelFactory levelFactory;

    public EnemySystem(LevelFactory lvlf) {
        super(Family.all(EnemyComponent.class).get());
        em = ComponentMapper.getFor(EnemyComponent.class);
        bodm = ComponentMapper.getFor(B2dBodyComponent.class);
        levelFactory = lvlf;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        final EnemyComponent enemyCom = em.get(entity); // get EnemyComponent
        final B2dBodyComponent bodyCom = bodm.get(entity); // get
                                                           // B2dBodyComponent

        if (enemyCom.enemyType == Type.DROPLET) {
            // get distance of enemy from its original start position (pad
            // center)
            final float distFromOrig = Math
                    .abs(enemyCom.xPosCenter - bodyCom.body.getPosition().x);

            // if distance > 1 swap direction
            enemyCom.isGoingLeft = (distFromOrig > 1) ? !enemyCom.isGoingLeft
                    : enemyCom.isGoingLeft;

            // set speed base on direction
            final float speed = enemyCom.isGoingLeft ? -0.01f : 0.01f;

            // apply speed to body
            bodyCom.body.setTransform(bodyCom.body.getPosition().x + speed,
                    bodyCom.body.getPosition().y, bodyCom.body.getAngle());
        } else if (enemyCom.enemyType == Type.CLOUD) {
            final B2dBodyComponent b2Player = Mapper.b2dCom
                    .get(levelFactory.player);
            final B2dBodyComponent b2Enemy = Mapper.b2dCom.get(entity);

            final float distance = b2Player.body.getPosition()
                    .dst(b2Enemy.body.getPosition());
            final SteeringComponent scom = Mapper.steerCom.get(entity);
            if (distance < 3
                    && scom.currentMode != SteeringComponent.SteeringState.FLEE) {
                scom.steeringBehavior = SteeringPresets.getFlee(
                        Mapper.steerCom.get(entity),
                        Mapper.steerCom.get(levelFactory.player));
                scom.currentMode = SteeringComponent.SteeringState.FLEE;
            } else if (distance > 3 && distance < 10
                    && scom.currentMode != SteeringComponent.SteeringState.ARRIVE) {
                scom.steeringBehavior = SteeringPresets.getArrive(
                        Mapper.steerCom.get(entity),
                        Mapper.steerCom.get(levelFactory.player));
                scom.currentMode = SteeringComponent.SteeringState.ARRIVE;
            } else if (distance > 15
                    && scom.currentMode != SteeringComponent.SteeringState.WANDER) {
                scom.steeringBehavior = SteeringPresets
                        .getWander(Mapper.steerCom.get(entity));
                scom.currentMode = SteeringComponent.SteeringState.WANDER;
            }

            // should enemy shoot
            if (scom.currentMode == SteeringComponent.SteeringState.ARRIVE) {
                // enemy is following
                if (enemyCom.timeSinceLastShot >= enemyCom.shootDelay) {
                    // do shoot
                    final Vector2 aim = DFUtils.aimTo(
                            bodyCom.body.getPosition(),
                            b2Player.body.getPosition());
                    aim.scl(3);
                    levelFactory.createBullet(bodyCom.body.getPosition().x,
                            bodyCom.body.getPosition().y, aim.x, aim.y,
                            BulletComponent.Owner.ENEMY);
                    // reset timer
                    enemyCom.timeSinceLastShot = 0;
                }
            }
        }

        // do shoot timer
        enemyCom.timeSinceLastShot += deltaTime;

        // check for dead enemies
        if (enemyCom.isDead) {
            bodyCom.isDead = true;
        }
    }

}