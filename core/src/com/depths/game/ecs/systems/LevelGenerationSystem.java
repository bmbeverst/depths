/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.depths.game.ecs.components.PlayerComponent;
import com.depths.game.ecs.components.TransformComponent;
import com.depths.game.physics.factory.LevelFactory;

public class LevelGenerationSystem extends IteratingSystem {

    // get transform component so we can check players height
    private final ComponentMapper<TransformComponent> tm = ComponentMapper
            .getFor(TransformComponent.class);
    private final LevelFactory lf;

    public LevelGenerationSystem(LevelFactory lvlFactory) {
        super(Family.all(PlayerComponent.class).get());
        lf = lvlFactory;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        final TransformComponent trans = tm.get(entity);
        final int currentPosition = (int) trans.position.y;
        if ((currentPosition + 7) > lf.currentLevel) {
            lf.generateLevel(currentPosition + 7);
        }
    }
}