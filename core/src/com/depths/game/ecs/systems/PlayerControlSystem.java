/*
 * MIT License
 *
 * Copyright (c) 2022 Brooks B
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.depths.game.controllers.KeyboardController;
import com.depths.game.ecs.components.B2dBodyComponent;
import com.depths.game.ecs.components.BulletComponent;
import com.depths.game.ecs.components.PlayerComponent;
import com.depths.game.ecs.components.StateComponent;
import com.depths.game.physics.factory.LevelFactory;
import com.depths.game.util.ParticleEffectManager;

public class PlayerControlSystem extends IteratingSystem {

    ComponentMapper<PlayerComponent> pm;
    ComponentMapper<B2dBodyComponent> bodm;
    ComponentMapper<StateComponent> sm;
    KeyboardController controller;
    private final LevelFactory lvlFactory;

    public PlayerControlSystem(KeyboardController keyCon, LevelFactory lvlf) {
        super(Family.all(PlayerComponent.class).get());
        lvlFactory = lvlf;
        controller = keyCon;
        pm = ComponentMapper.getFor(PlayerComponent.class);
        bodm = ComponentMapper.getFor(B2dBodyComponent.class);
        sm = ComponentMapper.getFor(StateComponent.class);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        final B2dBodyComponent b2body = bodm.get(entity);
        final StateComponent state = sm.get(entity);
        final PlayerComponent player = pm.get(entity);

        player.cam.position.y = b2body.body.getPosition().y;

        // make player jump very high
        if (player.onSpring && controller.up) {
            b2body.body.applyLinearImpulse(0, 75f,
                    b2body.body.getWorldCenter().x,
                    b2body.body.getWorldCenter().y, true);
            b2body.body.setTransform(b2body.body.getPosition().x,
                    b2body.body.getPosition().y + 5, b2body.body.getAngle());
            // add particle effect at feet
            lvlFactory.makeParticleEffect(ParticleEffectManager.Type.SMOKE,
                    b2body.body.getPosition().x, b2body.body.getPosition().y);
            state.set(StateComponent.STATE_JUMPING);
            player.onSpring = false;
        }

        float playerVelY = b2body.body.getLinearVelocity().y;
        float playerVelX = b2body.body.getLinearVelocity().x;

        if (playerVelY > 0 && state.get() != StateComponent.STATE_FALLING) {
            state.set(StateComponent.STATE_FALLING);
//            Gdx.app.debug(this.getClass().getSimpleName(), "FALL");
        }

        if (playerVelY == 0) {

//            Gdx.app.debug(this.getClass().getSimpleName(),
//                    "playerVelX = " + playerVelX);
//            Gdx.app.debug(this.getClass().getSimpleName(),
//                    "state = " + state.get());
            if (state.get() == StateComponent.STATE_FALLING) {
                state.set(StateComponent.STATE_NORMAL);
            }
            if (playerVelX > 0f
                    && state.get() != StateComponent.STATE_MOVING_RIGHT) {
                state.set(StateComponent.STATE_MOVING_RIGHT);
//                Gdx.app.debug(this.getClass().getSimpleName(), "RIGHT");
            }
            if (playerVelX < 0f
                    && state.get() != StateComponent.STATE_MOVING_LEFT) {
                state.set(StateComponent.STATE_MOVING_LEFT);
//                Gdx.app.debug(this.getClass().getSimpleName(), "LEFT");
            }
            if (playerVelX == 0f
                    && state.get() != StateComponent.STATE_NORMAL) {
                state.set(StateComponent.STATE_NORMAL);
//                Gdx.app.debug(this.getClass().getSimpleName(), "Normal!!!!");
            }
        }

        if (playerVelY < .01 && state.get() == StateComponent.STATE_FALLING) {
            // player is actually falling. check if they are on platform
            if (player.onPlatform) {
                // overwrite old y value with 0 t stop falling but keep x vel
                b2body.body.setLinearVelocity(playerVelX, 0f);
                player.onPlatform = true;
            }
        }

        if (controller.left) {
            b2body.body.setLinearVelocity(MathUtils.lerp(playerVelX, -5f, 0.2f),
                    playerVelY);
        }
        if (controller.right) {
            b2body.body.setLinearVelocity(MathUtils.lerp(playerVelX, 5f, 0.2f),
                    playerVelY);
        }

        if (!controller.left && !controller.right) {
            b2body.body.setLinearVelocity(MathUtils.lerp(playerVelX, 0, 0.1f),
                    playerVelY);
        }

        if (controller.up && (state.get() == StateComponent.STATE_NORMAL
                || state.get() == StateComponent.STATE_MOVING
                || state.get() == StateComponent.STATE_MOVING_LEFT
                || state.get() == StateComponent.STATE_MOVING_RIGHT)) {
            // b2body.body.applyForceToCenter(0, 3000,true);
            b2body.body.applyLinearImpulse(0, 75f,
                    b2body.body.getWorldCenter().x,
                    b2body.body.getWorldCenter().y, true);
            state.set(StateComponent.STATE_JUMPING);
            player.onPlatform = false;
//            Gdx.app.debug(this.getClass().getSimpleName(), "JUMP");
        }

        if (controller.escape) {
            player.isDead = true;
            // Can't reset this normally
            controller.escape = false;
        }

        if (player.timeSinceLastShot > 0) {
            player.timeSinceLastShot -= deltaTime;
        }

        if (controller.isMouse1Down) { // if mouse button is pressed
            // user wants to fire
            if (player.timeSinceLastShot <= 0) { // check the player hasn't just
                                                 // shot
                // reset timeSinceLastShot
                player.timeSinceLastShot = player.shootDelay;
                // player can shoot so do player shoot
                final Vector3 mousePos = new Vector3(controller.mouseLocation.x,
                        controller.mouseLocation.y, 0); // get mouse
                                                        // position
                player.cam.unproject(mousePos); // convert position from screen
                                                // to box2d world
                                                // position
                final float speed = 10f; // set the speed of the bullet
                final float shooterX = b2body.body.getPosition().x; // get
                                                                    // player
                                                                    // location
                final float shooterY = b2body.body.getPosition().y; // get
                                                                    // player
                                                                    // location
                float velx = mousePos.x - shooterX; // get distance from shooter
                                                    // to target on x
                                                    // plain
                float vely = mousePos.y - shooterY; // get distance from shooter
                                                    // to target on y
                                                    // plain
                final float length = (float) Math
                        .sqrt(velx * velx + vely * vely); // get distance to
                // target direct
                if (length != 0) {
                    velx = velx / length; // get required x velocity to aim at
                                          // target
                    vely = vely / length; // get required y velocity to aim at
                                          // target
                }
                // create a bullet
                lvlFactory.createBullet(shooterX, shooterY, velx * speed,
                        vely * speed, BulletComponent.Owner.PLAYER);

            }
        }
    }
}