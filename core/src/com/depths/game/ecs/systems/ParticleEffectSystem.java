/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.depths.game.ecs.components.Mapper;
import com.depths.game.ecs.components.ParticleEffectComponent;

public class ParticleEffectSystem extends IteratingSystem {

    private static final boolean shouldRender = true;

    private final Array<Entity> renderQueue;
    private final SpriteBatch batch;
    private final OrthographicCamera camera;

    public ParticleEffectSystem(SpriteBatch sb, OrthographicCamera cam) {
        super(Family.all(ParticleEffectComponent.class).get());
        renderQueue = new Array<>();
        batch = sb;
        camera = cam;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        batch.setProjectionMatrix(camera.combined);
        batch.enableBlending();
        // Render PE
        if (shouldRender) {
            batch.begin();
            for (final Entity entity : renderQueue) {
                final ParticleEffectComponent pec = Mapper.peCom.get(entity);
                pec.particleEffect.draw(batch, deltaTime);
            }
            batch.end();
        }
        renderQueue.clear();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        final ParticleEffectComponent pec = Mapper.peCom.get(entity);
        // Move PE if attached
        if (pec.isAttached) {
            pec.particleEffect.setPosition(
                    pec.attachedBody.getPosition().x + pec.xOffset,
                    pec.attachedBody.getPosition().y + pec.yOffset);
        }
        // free PE if completed
        if (pec.isDead || pec.particleEffect.isComplete()) {
            getEngine().removeEntity(entity);
        } else {
            renderQueue.add(entity);
        }
    }
}