/*
 * MIT License
 *
 * Copyright (c) 2022 Brooks B
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.depths.game.ecs.components.BulletComponent;
import com.depths.game.ecs.components.CollisionComponent;
import com.depths.game.ecs.components.EnemyComponent;
import com.depths.game.ecs.components.Mapper;
import com.depths.game.ecs.components.PlayerComponent;
import com.depths.game.ecs.components.TypeComponent;

public class CollisionSystem extends IteratingSystem {
    ComponentMapper<CollisionComponent> cm;
    ComponentMapper<PlayerComponent> pm;

    public CollisionSystem() {
        super(Family.all(CollisionComponent.class).get());

        cm = ComponentMapper.getFor(CollisionComponent.class);
        pm = ComponentMapper.getFor(PlayerComponent.class);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        // get collision for this entity
        final CollisionComponent cc = cm.get(entity);
        // get collided entity
        final Entity collidedEntity = cc.collisionEntity;

        final TypeComponent thisType = entity.getComponent(TypeComponent.class);

        // Do Player Collisions
        if (thisType.type == TypeComponent.Types.PLAYER) {
            if (collidedEntity != null) {
                final TypeComponent type = collidedEntity
                        .getComponent(TypeComponent.class);
                if (type != null) {
                    final PlayerComponent pl = pm.get(entity);
                    switch (type.type) {
                        case ENEMY:
//                            Gdx.app.debug(this.getClass().getSimpleName(),
//                                    "player hit enemy");
                            handle_enemy_player(entity);
                            break;
                        case SCENERY:
                            // do player hit scenery thing
                            pm.get(entity).onPlatform = true;
//						Gdx.app.debug(this.getClass().getSimpleName(), "player hit scenery");
                            break;
                        case SPRING:
                            // do player hit other thing
                            pm.get(entity).onSpring = true;
//                            Gdx.app.debug(this.getClass().getSimpleName(),
//                                    "player hit spring: bounce up");
                            break;
                        case OTHER:
                            // do player hit other thing
                            Gdx.app.debug(this.getClass().getSimpleName(),
                                    "player hit other");
                            break;
                        case BULLET:
                            final BulletComponent bullet = Mapper.bulletCom
                                    .get(collidedEntity);
                            player_bullet(pl, bullet);
                            break;
                        default:
                            Gdx.app.debug(this.getClass().getSimpleName(),
                                    "Player no matching type found");
                            break;
                    }
                    // collision handled reset component
                    cc.collisionEntity = null;
                } else {
                    Gdx.app.debug(this.getClass().getSimpleName(),
                            "Player type == null");
                }
            }

        }
        // Do enemy collisions
        if (thisType.type == TypeComponent.Types.ENEMY) {
            if (collidedEntity != null) {
                final TypeComponent type = collidedEntity
                        .getComponent(TypeComponent.class);

                if (type != null) {
//                    if (type.type != TypeComponent.Types.SCENERY
//                            || type.type != TypeComponent.Types.SPRING) {
//                        Gdx.app.debug(this.getClass().getSimpleName(),
//                                "enemy collided by " + type.type.name() + " at "
//                                        + deltaTime);
//                    }
                    switch (type.type) {
                        case PLAYER:
//                            Gdx.app.debug(this.getClass().getSimpleName(),
//                                    "enemy hit player");
                            handle_enemy_player(collidedEntity);
                            break;
                        case OTHER:
                            Gdx.app.debug(this.getClass().getSimpleName(),
                                    "enemy hit other");
                            break;
                        case BULLET:
                            final EnemyComponent enemy = Mapper.enemyCom
                                    .get(entity);
                            final BulletComponent bullet = Mapper.bulletCom
                                    .get(collidedEntity);
                            enemy_bullet(enemy, bullet);
                            break;
                        default:
//                            Gdx.app.error(this.getClass().getSimpleName(),
//                                    "Enemy no matching type found");
                            break;
                    }
                    // collision handled reset component
                    cc.collisionEntity = null;
                } else {
                    Gdx.app.error(this.getClass().getSimpleName(),
                            "Enemy type == null");
                }
            }
        }
        // Do enemy collisions
        if (thisType.type == TypeComponent.Types.BULLET) {
            if (collidedEntity != null) {
                final TypeComponent type = collidedEntity
                        .getComponent(TypeComponent.class);
                final BulletComponent bullet = Mapper.bulletCom.get(entity);
                if (type != null) {
//                    if (type.type != TypeComponent.Types.SCENERY) {
//                        Gdx.app.debug(this.getClass().getSimpleName(),
//                                "bullet of " + bullet.owner.name()
//                                        + " collided by " + type.type.name()
//                                        + " at " + deltaTime);
//                    }
                    switch (type.type) {
                        case BULLET:
                            final BulletComponent bullet2 = Mapper.bulletCom
                                    .get(collidedEntity);
                            // Players bullets win always
                            if (bullet.owner == BulletComponent.Owner.PLAYER) {
                                bullet2.isDead = true;
                            }
                            if (bullet2.owner == BulletComponent.Owner.PLAYER) {
                                bullet.isDead = true;
                            }
                            break;
                        case ENEMY:
                            final EnemyComponent enemy = Mapper.enemyCom
                                    .get(collidedEntity);
                            enemy_bullet(enemy, bullet);
                            break;
                        case PLAYER:
                            final PlayerComponent pl = pm.get(collidedEntity);
                            player_bullet(pl, bullet);
                            break;
                        default:
//                            Gdx.app.error(this.getClass().getSimpleName(),
//                                    "Bullet no matching type found");
                            break;
                    }

                    // collision handled reset component
                    cc.collisionEntity = null;
                } else {
                    Gdx.app.error(this.getClass().getSimpleName(),
                            "Bullet type == null");
                }
            }
        }
    }

    private void player_bullet(final PlayerComponent pl,
            final BulletComponent bullet) {
        // can't shoot own team
        if (bullet.owner != BulletComponent.Owner.PLAYER) {
            bullet.isDead = true;
            pl.isDead = true;
//            Gdx.app.debug(this.getClass().getSimpleName(),
//                    "player hit by bullet");
        }
//        Gdx.app.debug(this.getClass().getSimpleName(),
//                "player got Hit by " + bullet.owner.name());
    }

    private void enemy_bullet(final EnemyComponent enemy,
            final BulletComponent bullet) {
//        Gdx.app.debug(this.getClass().getSimpleName(),
//                "enemy got Hit by " + bullet.owner.name());
        // can't shoot own team
        if (bullet.owner != BulletComponent.Owner.ENEMY) {
            enemy.isDead = true;
//            Gdx.app.debug(this.getClass().getSimpleName(), "enemy got shot");
        }
    }

    private void handle_enemy_player(Entity entity) {
        // do player hit enemy thing
        final PlayerComponent pl = pm.get(entity);
        pl.isDead = true;
        final int score = (int) pl.cam.position.y;
        Gdx.app.log(this.getClass().getSimpleName(), "Score = " + score);
    }
}