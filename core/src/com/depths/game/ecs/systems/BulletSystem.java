/*
 * MIT License
 *
 * Copyright (c) 2022 Brooks B
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.depths.game.ecs.components.B2dBodyComponent;
import com.depths.game.ecs.components.BulletComponent;
import com.depths.game.ecs.components.Mapper;
import com.depths.game.ecs.components.ParticleEffectComponent;
import com.depths.game.physics.factory.LevelFactory;

public class BulletSystem extends IteratingSystem {
    private final int max_distance = 20;
    private final LevelFactory lvlFactory;

    public BulletSystem(LevelFactory lvlFactory) {
        super(Family.all(BulletComponent.class).get());
        this.lvlFactory = lvlFactory;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        // get box 2d body and bullet components
        final B2dBodyComponent b2body = Mapper.b2dCom.get(entity);
        final BulletComponent bullet = Mapper.bulletCom.get(entity);

        // apply bullet velocity to bullet body
        b2body.body.setLinearVelocity(bullet.xVel, bullet.yVel);

        // get player pos
        final B2dBodyComponent playerBodyComp = Mapper.b2dCom
                .get(lvlFactory.player);
        final float px = playerBodyComp.body.getPosition().x;
        final float py = playerBodyComp.body.getPosition().y;

        // get bullet pos
        final float bx = b2body.body.getPosition().x;
        final float by = b2body.body.getPosition().y;

        // if bullet is 20 units away from player on any axis then it is
        // probably off
        // screen
        // TODO base max_distance on screen size
        if (Math.abs(bx - px) > max_distance
                || Math.abs(by - py) > max_distance) {
            bullet.isDead = true;
        }

        // check if bullet is dead
        if (bullet.isDead) {
            // This check for null is since the ParticleEffectSystem can
            // get to removing the effect first, I think
            // Setting the pec isDead is to fix the bug where the ParticleEffect
            // can be duplicated.
            final ParticleEffectComponent pec = Mapper.peCom
                    .get(bullet.particleEffect);
            if (pec != null) {
                pec.isDead = true;
            } else {
//                Gdx.app.debug(this.getClass().getSimpleName(),
//                        "Particle effect arleady dead");
            }
            b2body.isDead = true;
        }
    }
}