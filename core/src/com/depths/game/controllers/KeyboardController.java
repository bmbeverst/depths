/*
 * MIT License
 *
 * Copyright (c) 2022 Brooks B
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.controllers;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

public class KeyboardController implements InputProcessor {

    public boolean left, right, up, down, escape;
    public boolean isMouse1Down, isMouse2Down, isMouse3Down;
    public boolean isDragged;
    public Vector2 mouseLocation = new Vector2(0, 0);

    // This is activated once when a key on the keyboard is pressed down.
    @Override
    public boolean keyDown(int keycode) {
        boolean keyProcessed = false;
        switch (keycode) // switch code base on the variable keycode
        {
            case Keys.LEFT: // if keycode is the same as Keys.LEFT a.k.a 21
                left = true; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.RIGHT: // if keycode is the same as Keys.LEFT a.k.a 22
                right = true; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.UP: // if keycode is the same as Keys.LEFT a.k.a 19
                up = true; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.DOWN: // if keycode is the same as Keys.LEFT a.k.a 20
                down = true; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.ESCAPE: // if keycode is the same as Keys.ESCAPE a.k.a 131
                escape = true; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
        }
        return keyProcessed; // return our peyProcessed flag
    }

    // This as activated once when the key that was pressed down is released.
    @Override
    public boolean keyUp(int keycode) {
        boolean keyProcessed = false;
        switch (keycode) // switch code base on the variable keycode
        {
            case Keys.LEFT: // if keycode is the same as Keys.LEFT a.k.a 21
                left = false; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.RIGHT: // if keycode is the same as Keys.LEFT a.k.a 22
                right = false; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.UP: // if keycode is the same as Keys.LEFT a.k.a 19
                up = false; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.DOWN: // if keycode is the same as Keys.LEFT a.k.a 20
                down = false; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
            case Keys.ESCAPE: // if keycode is the same as Keys.ESCAPE a.k.a 131
                escape = false; // do this
                keyProcessed = true; // we have reacted to a keypress
                break;
        }
        return keyProcessed; // return our peyProcessed flag
    }

    // This is activated everytime the keyboard sends a character. Unlike KeyUp
    // and
    // KeyDown this will be called many times while the key is down.
    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    // Called when a mouse button or finger(on phone) is pressed down.
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer,
            int button) {
        if (button == 0) {
            isMouse1Down = true;
        } else if (button == 1) {
            isMouse2Down = true;
        } else if (button == 2) {
            isMouse3Down = true;
        }
        mouseLocation.x = screenX;
        mouseLocation.y = screenY;
        return false;
    }

    // Called when a finger or mouse button is released.
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        isDragged = false;
        // System.out.println(button);
        if (button == 0) {
            isMouse1Down = false;
        } else if (button == 1) {
            isMouse2Down = false;
        } else if (button == 2) {
            isMouse3Down = false;
        }
        mouseLocation.x = screenX;
        mouseLocation.y = screenY;
        return false;
    }

    // Called each time the finger or mouse moves while in the down state.
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        isDragged = true;
        mouseLocation.x = screenX;
        mouseLocation.y = screenY;
        return false;
    }

    // Called when the mouse moves whether a button is pressed or not.
    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        mouseLocation.x = screenX;
        mouseLocation.y = screenY;
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        // TODO Auto-generated method stub
        return false;
    }

}
