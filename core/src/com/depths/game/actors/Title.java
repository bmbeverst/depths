/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * The Class Title.
 */
public class Title extends Actor {

    private final AtlasRegion image;
    private final BitmapFont font;
    private String text = "Starting";

    /**
     * Instantiates a new title text for the actor
     *
     * @param ar the AtlasRegion of the image to use for the Title
     */
    public Title(AtlasRegion ar) {
        super();
        image = ar;
        setWidth(80);
        setHeight(100);
        font = new BitmapFont(Gdx.files.internal("arial-15.fnt"), false);
    }

    /**
     * Draw this actor with text and image.
     *
     * @param batch       the batch to use for drawing
     * @param parentAlpha the parent alpha
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(image, getX(), getY(), 80, 80);
        font.draw(batch, "Welcome to Depths!!!", getX() - 35, getY() - 10);
        font.draw(batch, text, getX() - text.length(), getY() - 25);
        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
    }

    /**
     * Sets the text of the Title
     *
     * @param text the new text
     */
    public void setText(String text) {
        this.text = text;
    }

}
