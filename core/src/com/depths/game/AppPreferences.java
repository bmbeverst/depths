/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * The Class AppPreferences.
 *
 * App preferences for a game
 *
 * @author brooks
 */
public class AppPreferences {

    private static final String PREF_MUSIC_VOLUME = "volume";
    private static final String PREF_MUSIC_ENABLED = "music.enabled";
    private static final String PREF_SOUND_VOLUME = "sound";
    private static final String PREF_SOUND_ENABLED = "sound.enabled";
    private static final String PREF_DEBUG_ENABLED = "debug.enabled";
    private static final String PREFS_NAME = "lappel-du-vibe";

    /**
     * Gets the Gdx Preferences.
     *
     * @return the Gdx Preferences
     */
    protected Preferences getPrefs() {
        return Gdx.app.getPreferences(PREFS_NAME);
    }

    /**
     * Return if the sound effects are enabled.
     *
     * @return true if sound effects are enabled, false otherwise
     */
    public boolean isSoundEffectsEnabled() {
        return getPrefs().getBoolean(PREF_SOUND_ENABLED, true);
    }

    /**
     * Set sound effect.
     *
     * @param soundEffectsEnabled What to set the sound effects to
     */
    public void setSoundEffects(boolean soundEffectsEnabled) {
        getPrefs().putBoolean(PREF_SOUND_ENABLED, soundEffectsEnabled);
        getPrefs().flush();
    }

    /**
     * Return if debug is enabled.
     *
     * @return true if debug are enabled, false otherwise
     */
    public boolean isDebugEnabled() {
        return getPrefs().getBoolean(PREF_DEBUG_ENABLED, false);
    }

    /**
     * Set debug.
     *
     * @param debugEnabled What to set the debug to
     */
    public void setDebug(boolean debugEnabled) {
        getPrefs().putBoolean(PREF_DEBUG_ENABLED, debugEnabled);
        getPrefs().flush();
    }

    /**
     * Checks if is music enabled.
     *
     * @return true, if is music enabled
     */
    public boolean isMusicEnabled() {
        return getPrefs().getBoolean(PREF_MUSIC_ENABLED, true);
    }

    /**
     * Sets the music.
     *
     * @param musicEnabled the new music
     */
    public void setMusic(boolean musicEnabled) {
        getPrefs().putBoolean(PREF_MUSIC_ENABLED, musicEnabled);
        getPrefs().flush();
    }

    /**
     * Gets the music volume.
     *
     * @return the music volume
     */
    public float getMusicVolume() {
        return getPrefs().getFloat(PREF_MUSIC_VOLUME, 0.5f);
    }

    /**
     * Sets the music volume.
     *
     * @param musicVolume the new music volume
     */
    public void setMusicVolume(float musicVolume) {
        getPrefs().putFloat(PREF_MUSIC_VOLUME, musicVolume);
        getPrefs().flush();
    }

    /**
     * Gets the sound volume.
     *
     * @return the sound volume
     */
    public float getSoundVolume() {
        return getPrefs().getFloat(PREF_SOUND_VOLUME, 0.5f);
    }

    /**
     * Sets the sound volume.
     *
     * @param soundVolume the new sound volume
     */
    public void setSoundVolume(float soundVolume) {
        getPrefs().putFloat(PREF_SOUND_VOLUME, soundVolume);
        getPrefs().flush();
    }
}