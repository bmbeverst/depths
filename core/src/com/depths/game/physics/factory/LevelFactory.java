/*
 * MIT License
 *
 * Copyright (c) 2022 Brooks B
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game.physics.factory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.depths.game.ai.SteeringPresets;
import com.depths.game.ecs.components.AnimationComponent;
import com.depths.game.ecs.components.B2dBodyComponent;
import com.depths.game.ecs.components.BulletComponent;
import com.depths.game.ecs.components.BulletComponent.Owner;
import com.depths.game.ecs.components.CollisionComponent;
import com.depths.game.ecs.components.EnemyComponent;
import com.depths.game.ecs.components.LightComponent;
import com.depths.game.ecs.components.ParticleEffectComponent;
import com.depths.game.ecs.components.PlayerComponent;
import com.depths.game.ecs.components.StateComponent;
import com.depths.game.ecs.components.SteeringComponent;
import com.depths.game.ecs.components.TextureComponent;
import com.depths.game.ecs.components.TransformComponent;
import com.depths.game.ecs.components.TypeComponent;
import com.depths.game.ecs.systems.RenderingSystem;
import com.depths.game.loader.DepthsAssetManager;
import com.depths.game.physics.B2dContactListener;
import com.depths.game.util.DFUtils;
import com.depths.game.util.OpenSimplexNoise;
import com.depths.game.util.ParticleEffectManager;
import com.depths.game.util.ParticleEffectManager.Type;

import box2dLight.PointLight;
import box2dLight.RayHandler;

public class LevelFactory {
    private final BodyFactory bodyFactory;
    public World world;
    private final PooledEngine engine;
//	private SimplexNoise sim;
    public int currentLevel = 0;
    private final TextureRegion floorTex;
    private final Animation<TextureRegion> enemyTex;
    private final Animation<TextureRegion> telportTex;
    // private SimplexNoise simRough;
    private final TextureAtlas atlas;
    private OpenSimplexNoise openSim;
    private final ParticleEffectManager pem;
    public Entity player;
    private final TextureRegion bulletTex;
    public final RayHandler rayHandler;

    static final int RAYS_PER_BALL = 128;
    static final int BALLSNUM = 5;
    static final float LIGHT_DISTANCE = 16f;
    static final float RADIUS = 1f;

    public LevelFactory(PooledEngine en, DepthsAssetManager assetMgr) {
        engine = en;
        atlas = assetMgr.manager.get("images/depthsGame.atlas");
        floorTex = atlas.findRegion("platform");
        telportTex = new Animation<TextureRegion>(0.1f,
                atlas.findRegions("teleporter_effect/teleport"),
                PlayMode.LOOP_REVERSED);
        enemyTex = new Animation<TextureRegion>(0.1f,
                atlas.findRegions("shadowMonster/ShadownMonster"));
        bulletTex = DFUtils.makeTextureRegion(10, 10, "FF5733FF");
        DFUtils.makeTextureRegion(2 * RenderingSystem.PPM,
                0.1f * RenderingSystem.PPM, "221122FF");
        world = new World(new Vector2(0, -10f), true);
        world.setContactListener(new B2dContactListener());
        bodyFactory = BodyFactory.getInstance(world);

        openSim = new OpenSimplexNoise(MathUtils.random(2000l));

        pem = new ParticleEffectManager();
        // TODO re-write this so that the Manger also has the file path!
        pem.addParticleEffect(ParticleEffectManager.Type.FIRE,
                assetMgr.manager.get("particles/fire.pe", ParticleEffect.class),
                1f / 64f);
        pem.addParticleEffect(ParticleEffectManager.Type.WATER, assetMgr.manager
                .get("particles/water.pe", ParticleEffect.class), 1f / 64f);
        pem.addParticleEffect(ParticleEffectManager.Type.SMOKE, assetMgr.manager
                .get("particles/smoke.pe", ParticleEffect.class), 1f / 64f);
        pem.addParticleEffect(
                ParticleEffectManager.Type.BLUEFIRE, assetMgr.manager
                        .get("particles/blueFire.pe", ParticleEffect.class),
                1f / 64f);
        pem.addParticleEffect(
                ParticleEffectManager.Type.LIGHTBEAM, assetMgr.manager
                        .get("particles/lightBeam.pe", ParticleEffect.class),
                1f / 64f);

        RayHandler.setGammaCorrection(true);
        RayHandler.useDiffuseLight(true);

        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(0f, 0f, 0f, 0.5f);
        rayHandler.setBlurNum(3);

    }

    /**
     * Creates a pair of platforms per level up to yLevel
     *
     * @param ylevel
     */
    public void generateLevel(int ylevel) {
        while (ylevel > currentLevel) {
            for (int i = 1; i < 5; i++) {
                generateSingleColumn(i);
            }
            currentLevel++;
        }
    }

    private void generateSingleColumn(int i) {
        final int offset = 10 * i;
        final int range = 15;
        if (genNForL(i, currentLevel) > -0.5f) {
            createPlatform(genNForL(i * 100, currentLevel) * range + offset,
                    currentLevel * 2);
            if (genNForL(i * 200, currentLevel) > 0.3f) {
                // add bouncy platform
                createBouncyPlatform(
                        genNForL(i * 100, currentLevel) * range + offset,
                        currentLevel * 2);
            }
            // only make enemies above level 7 (stops insta deaths)
            if (currentLevel > 7) {
                if (genNForL(i * 300, currentLevel) > 0.2f) {
                    // add an enemy
                    createEnemy(enemyTex,
                            genNForL(i * 100, currentLevel) * range + offset,
                            currentLevel * 2 + 1);
                }
            }
            // only make cloud enemies above level 10 (stops insta deaths)
            if (currentLevel > 10) {
                if (genNForL(i * 400, currentLevel) > 0.3f) {
                    // add a cloud enemy
                    createSeeker(
                            genNForL(i * 100, currentLevel) * range + offset,
                            currentLevel * 2 + 1);
                }
            }
        }
    }

    /**
     * Attache particle effect to body from body component
     *
     * @param  type    the type of particle effect to show
     * @param  b2dbody the bodycomponent with the body to attach to
     * @return         the Particle Effect Entity
     */
    public Entity makeParticleEffect(Type type, B2dBodyComponent b2dbody) {
        return makeParticleEffect(type, b2dbody, 0, 0);
    }

    /**
     * Make particle effect at xy
     *
     * @param  x
     * @param  y
     * @return   the Particle Effect Entity
     */
    public Entity makeParticleEffect(Type type, float x, float y) {
        final Entity entPE = engine.createEntity();
        final ParticleEffectComponent pec = engine
                .createComponent(ParticleEffectComponent.class);
        pec.particleEffect = pem.getPooledParticleEffect(type);
        pec.particleEffect.setPosition(x, y);
        entPE.add(pec);
        engine.addEntity(entPE);
        return entPE;
    }

    /**
     * Attache particle effect to body from body component with offsets
     *
     * @param  type    the type of particle effect to show
     * @param  b2dbody the bodycomponent with the body to attach to
     * @param  xo      x offset
     * @param  yo      y offset
     * @return         the Particle Effect Entity
     */
    public Entity makeParticleEffect(Type type, B2dBodyComponent b2dbody,
            float xo, float yo) {
        final Entity entPE = engine.createEntity();
        final ParticleEffectComponent pec = engine
                .createComponent(ParticleEffectComponent.class);
        pec.particleEffect = pem.getPooledParticleEffect(type);
        pec.particleEffect.setPosition(b2dbody.body.getPosition().x,
                b2dbody.body.getPosition().y);
        pec.xOffset = xo;
        pec.yOffset = yo;
        pec.isAttached = true;
        pec.attachedBody = b2dbody.body;

        final ParticleEmitter emitter = pec.particleEffect.getEmitters()
                .first();
        emitter.setAttached(true); // manually attach for testing
        emitter.setContinuous(true);

        // This makes it so that the fire comes out of the back!
        final Vector2 linearVel = b2dbody.body.getLinearVelocity();
        float angle = (float) Math.atan2(linearVel.y, linearVel.x);
        angle = angle * MathUtils.radiansToDegrees;
        angle += 180;

        emitter.getAngle().setHighMax(angle + 45f);
        emitter.getAngle().setHighMin(angle - 45f);
        emitter.getAngle().setLowMax(angle);
        emitter.getAngle().setLowMin(angle);

        if (type == Type.LIGHTBEAM) {
            emitter.getAngle().setHighMax(angle);
            emitter.getAngle().setHighMin(angle);
            emitter.getAngle().setLowMax(angle);
            emitter.getAngle().setLowMin(angle);
        }

        entPE.add(pec);
        engine.addEntity(entPE);

        return entPE;
    }

    // generate noise for level
    private float genNForL(int level, int height) {
        return (float) openSim.eval(height, level);
    }

    private void generateSingleColumn(float n1, float n2, float n3, float n4,
            int range, int offset) {
        if (n1 > -0.8f) {
            createPlatform(n2 * range + offset, currentLevel * 2);
            if (n3 > 0.3f) {
                // add bouncy platform
                createBouncyPlatform(n2 * range + offset, currentLevel * 2);
            }
            if (n4 > 0.2f) {
                // add an enemy
                createEnemy(enemyTex, n2 * range + offset,
                        currentLevel * 2 + 1);
            }
        }
    }

    public Entity createBullet(float x, float y, float xVel, float yVel,
            Owner owner) {
        final Entity entity = engine.createEntity();
        final B2dBodyComponent b2dbody = engine
                .createComponent(B2dBodyComponent.class);
        final TransformComponent position = engine
                .createComponent(TransformComponent.class);
        final TextureComponent texture = engine
                .createComponent(TextureComponent.class);
        final TypeComponent type = engine.createComponent(TypeComponent.class);
        final CollisionComponent colComp = engine
                .createComponent(CollisionComponent.class);
        final BulletComponent bul = engine
                .createComponent(BulletComponent.class);

        b2dbody.body = bodyFactory.makeCirclePolyBody(x, y, 0.5f,
                BodyFactory.Materials.STONE, BodyType.DynamicBody, true);
        // increase physics computation to limit body traveling through other
        // objects
        b2dbody.body.setBullet(true);
        // make bullets sensors so they don't move player
        bodyFactory.makeAllFixturesSensors(b2dbody.body);
        position.position.set(x, y, 0);
        texture.region = bulletTex;
        type.type = TypeComponent.Types.BULLET;
        b2dbody.body.setUserData(entity);
        bul.xVel = xVel;
        bul.yVel = yVel;

        // Set the velocity now so we can get particles moving the right way
        b2dbody.body.setLinearVelocity(xVel, yVel);
        // attach party to bullet
        if (owner == Owner.PLAYER) {
            bul.particleEffect = makeParticleEffect(
                    ParticleEffectManager.Type.LIGHTBEAM, b2dbody);
        } else {
            bul.particleEffect = makeParticleEffect(
                    ParticleEffectManager.Type.BLUEFIRE, b2dbody);
        }
        bul.owner = owner;

        entity.add(bul);
        entity.add(colComp);
        entity.add(b2dbody);
        entity.add(position);
        entity.add(texture);
        entity.add(type);

        add_point_light(b2dbody);

        engine.addEntity(entity);
        return entity;
    }

    public Entity createEnemy(Animation<TextureRegion> anim, float x, float y) {
        final Entity entity = engine.createEntity();
        final B2dBodyComponent b2dbody = engine
                .createComponent(B2dBodyComponent.class);
        final TransformComponent position = engine
                .createComponent(TransformComponent.class);
        final TextureComponent texture = engine
                .createComponent(TextureComponent.class);
        final AnimationComponent animCom = engine
                .createComponent(AnimationComponent.class);
        final EnemyComponent enemy = engine
                .createComponent(EnemyComponent.class);
        final TypeComponent type = engine.createComponent(TypeComponent.class);
        final StateComponent stateCom = engine
                .createComponent(StateComponent.class);
        final CollisionComponent colComp = engine
                .createComponent(CollisionComponent.class);

        b2dbody.body = bodyFactory.makeCirclePolyBody(x, y, 1,
                BodyFactory.Materials.STONE, BodyType.KinematicBody, true);
        position.position.set(x, y, 0);

        // anim.setPlayMode(Animation.PlayMode.LOOP);
        animCom.animations.put(StateComponent.STATE_NORMAL, anim);

        enemy.xPosCenter = x;
        enemy.enemyType = EnemyComponent.Type.DROPLET;
        type.type = TypeComponent.Types.ENEMY;
        stateCom.set(StateComponent.STATE_NORMAL);
        stateCom.isLooping = true;
        b2dbody.body.setUserData(entity);

        entity.add(colComp);
        entity.add(b2dbody);
        entity.add(position);
        entity.add(texture);
        entity.add(animCom);
        entity.add(enemy);
        entity.add(type);
        entity.add(stateCom);

        engine.addEntity(entity);

        return entity;
    }

    public Entity createBouncyPlatform(float x, float y) {
        final Entity entity = engine.createEntity();
        final B2dBodyComponent b2dbody = engine
                .createComponent(B2dBodyComponent.class);
        final TransformComponent position = engine
                .createComponent(TransformComponent.class);
        final TextureComponent texture = engine
                .createComponent(TextureComponent.class);
        final AnimationComponent animCom = engine
                .createComponent(AnimationComponent.class);
        final TypeComponent type = engine.createComponent(TypeComponent.class);
        final StateComponent stateCom = engine
                .createComponent(StateComponent.class);

        b2dbody.body = bodyFactory.makeBoxPolyBody(Math.abs(x), y + .26f, .5f,
                0.5f, BodyFactory.Materials.STONE, BodyType.StaticBody);
        // make it a sensor so not to impede movement
        bodyFactory.makeAllFixturesSensors(b2dbody.body);
        position.position.set(x, y, 0);
        texture.region = floorTex;
        texture.offsetY = -20f;
        animCom.animations.put(StateComponent.STATE_NORMAL, telportTex);
        type.type = TypeComponent.Types.SPRING;
        stateCom.set(StateComponent.STATE_NORMAL);
        stateCom.isLooping = true;
        b2dbody.body.setUserData(entity);

        entity.add(b2dbody);
        entity.add(position);
        entity.add(texture);
        entity.add(animCom);
        entity.add(type);
        entity.add(stateCom);

        engine.addEntity(entity);

        return entity;
    }

    public void createPlatform(float x, float y) {
        final Entity entity = engine.createEntity();
        final B2dBodyComponent b2dbody = engine
                .createComponent(B2dBodyComponent.class);
        b2dbody.body = bodyFactory.makeBoxPolyBody(x, y, 1.2f, 0.3f,
                BodyFactory.Materials.STONE, BodyType.StaticBody);
        b2dbody.body.setUserData(entity);
        entity.add(b2dbody);

        final TextureComponent texture = engine
                .createComponent(TextureComponent.class);
        texture.region = floorTex;
        entity.add(texture);

        final TypeComponent type = engine.createComponent(TypeComponent.class);
        type.type = TypeComponent.Types.SCENERY;
        entity.add(type);

        final TransformComponent trans = engine
                .createComponent(TransformComponent.class);
        trans.position.set(x, y, 0);
        entity.add(trans);

        engine.addEntity(entity);

    }

    public void createFloor() {
        final Entity entity = engine.createEntity();
        final B2dBodyComponent b2dbody = engine
                .createComponent(B2dBodyComponent.class);
        b2dbody.body = bodyFactory.makeBoxPolyBody(0, 0, 100, 0.2f,
                BodyFactory.Materials.STONE, BodyType.StaticBody);
        final TextureComponent texture = engine
                .createComponent(TextureComponent.class);
        texture.region = floorTex;
        final TypeComponent type = engine.createComponent(TypeComponent.class);
        type.type = TypeComponent.Types.SCENERY;

        b2dbody.body.setUserData(entity);

        entity.add(b2dbody);
        entity.add(texture);
        entity.add(type);

        engine.addEntity(entity);
    }

    public Entity createPlayer(OrthographicCamera cam) {

        final Entity entity = engine.createEntity();
        final B2dBodyComponent b2dbody = engine
                .createComponent(B2dBodyComponent.class);
        final TransformComponent position = engine
                .createComponent(TransformComponent.class);
        final TextureComponent texture = engine
                .createComponent(TextureComponent.class);
        final AnimationComponent animCom = engine
                .createComponent(AnimationComponent.class);
        final PlayerComponent player = engine
                .createComponent(PlayerComponent.class);
        final CollisionComponent colComp = engine
                .createComponent(CollisionComponent.class);
        final TypeComponent type = engine.createComponent(TypeComponent.class);
        final StateComponent stateCom = engine
                .createComponent(StateComponent.class);
        final SteeringComponent scom = engine
                .createComponent(SteeringComponent.class);

//        texture.offsetY = -30f;
        player.cam = cam;
        b2dbody.body = bodyFactory.makeCirclePolyBody(10, 1, 1,
                BodyFactory.Materials.STONE, BodyType.DynamicBody, true);
        // set object position (x,y,z) z used to define draw order 0 first drawn
        position.position.set(10, 1, 0);

        String animSet = "mech/";

        final Animation<TextureRegion> idle = new Animation<TextureRegion>(0.5f,
                atlas.findRegions(animSet + "idle"));
        final Animation<TextureRegion> fall = new Animation<TextureRegion>(0.5f,
                atlas.findRegions(animSet + "fall"));
        final Animation<TextureRegion> walkLeft = new Animation<TextureRegion>(
                0.2f, atlas.findRegions(animSet + "walkLeft"));
        final Animation<TextureRegion> walkRight = new Animation<TextureRegion>(
                0.2f, atlas.findRegions(animSet + "walkRight"));

        // anim.setPlayMode(Animation.PlayMode.LOOP);
        animCom.animations.put(StateComponent.STATE_NORMAL, idle);
        animCom.animations.put(StateComponent.STATE_MOVING, idle);
        animCom.animations.put(StateComponent.STATE_MOVING_LEFT, walkLeft);
        animCom.animations.put(StateComponent.STATE_MOVING_RIGHT, walkRight);
        animCom.animations.put(StateComponent.STATE_JUMPING, fall);
        animCom.animations.put(StateComponent.STATE_FALLING, fall);
        animCom.animations.put(StateComponent.STATE_HIT, idle);

        type.type = TypeComponent.Types.PLAYER;
        stateCom.set(StateComponent.STATE_NORMAL);
        stateCom.isLooping = true;
        b2dbody.body.setUserData(entity);

        scom.body = b2dbody.body;

        entity.add(b2dbody);
        entity.add(position);
        entity.add(texture);
        entity.add(animCom);
        entity.add(player);
        entity.add(colComp);
        entity.add(type);
        entity.add(stateCom);
        entity.add(scom);

        add_point_light(b2dbody);

        engine.addEntity(entity);
        this.player = entity;
        return entity;

    }

    /**
     * Adds a point light to the body
     *
     * @param  b2dbody
     * @return
     * @return
     */
    private void add_point_light(B2dBodyComponent b2dbody) {
        final Entity entLi = engine.createEntity();
        final LightComponent lightComp = engine
                .createComponent(LightComponent.class);
        final PointLight light = new PointLight(rayHandler, RAYS_PER_BALL,
                Color.WHITE, LIGHT_DISTANCE, 0f, 0f);
        light.attachToBody(b2dbody.body, RADIUS / 2f, RADIUS / 2f);
        light.setColor(MathUtils.random(), MathUtils.random(),
                MathUtils.random(), 1f);
        lightComp.b2dlight = light;
        lightComp.b2dbody = b2dbody;
        entLi.add(lightComp);
        engine.addEntity(entLi);
    }

    public Entity createSeeker(float x, float y) {
        final Entity entity = engine.createEntity();
        final B2dBodyComponent b2dbody = engine
                .createComponent(B2dBodyComponent.class);
        final TransformComponent position = engine
                .createComponent(TransformComponent.class);
        final TextureComponent texture = engine
                .createComponent(TextureComponent.class);
        final CollisionComponent colComp = engine
                .createComponent(CollisionComponent.class);
        final TypeComponent type = engine.createComponent(TypeComponent.class);
        final StateComponent stateCom = engine
                .createComponent(StateComponent.class);
        final EnemyComponent enemy = engine
                .createComponent(EnemyComponent.class);
        final SteeringComponent scom = engine
                .createComponent(SteeringComponent.class);

        b2dbody.body = bodyFactory.makeCirclePolyBody(x, y, 1,
                BodyFactory.Materials.STONE, BodyType.DynamicBody, true);
        b2dbody.body.setGravityScale(0f); // no gravity for our floating enemy
        b2dbody.body.setLinearDamping(0.3f); // setting linear dampening so the
                                             // enemy slows down in
                                             // our box2d world(or it can float
                                             // on forever)

        position.position.set(x, y, 0);
        texture.region = atlas.findRegion("player");
        type.type = TypeComponent.Types.ENEMY;
        stateCom.set(StateComponent.STATE_NORMAL);
        enemy.enemyType = EnemyComponent.Type.CLOUD;
        b2dbody.body.setUserData(entity);
        bodyFactory.makeAllFixturesSensors(b2dbody.body); // seeker should fly
                                                          // about not fall
        scom.body = b2dbody.body;
        // enemy.enemyType = EnemyComponent.Type.CLOUD; // used later in
        // tutorial

        // set out steering behavior
        scom.steeringBehavior = SteeringPresets.getWander(scom);
        scom.currentMode = SteeringComponent.SteeringState.WANDER;

        entity.add(b2dbody);
        entity.add(position);
        entity.add(texture);
        entity.add(colComp);
        entity.add(type);
        entity.add(enemy);
        entity.add(stateCom);
        entity.add(scom);

        engine.addEntity(entity);
        return entity;
    }

    public void resetWorld() {
        currentLevel = 0;
        openSim = new OpenSimplexNoise(MathUtils.random(2000l));
        final Array<Body> bods = new Array<>();
        world.getBodies(bods);
        for (final Body bod : bods) {
            world.destroyBody(bod);
        }

    }
}