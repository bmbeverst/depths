/*
 * MIT License
 * 
 * Copyright (c) 2022 Brooks B
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.depths.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.depths.game.loader.DepthsAssetManager;
import com.depths.game.screens.EndScreen;
import com.depths.game.screens.GameScreen;
import com.depths.game.screens.LoadingScreen;
import com.depths.game.screens.MenuScreen;
import com.depths.game.screens.PreferencesScreen;
import com.depths.game.screens.Screens;

/**
 * The Class Depths that deals with all the game related screens
 */
public class Depths extends Game {

    private LoadingScreen loadingScreen;
    private PreferencesScreen preferencesScreen;
    private MenuScreen menuScreen;
    private GameScreen mainScreen;
    private EndScreen endScreen;
    private AppPreferences preferences;
    /**
     * The asset manager to be used for loading assets
     */
    public DepthsAssetManager assetManager = new DepthsAssetManager();
    private Music playingSong;
    /**
     * The score of the player in the last game
     */
    public int lastScore;

    @Override
    public void create() {
        Gdx.app.debug(this.getClass().getSimpleName(),
                "Welcome to Depths debug");
        loadingScreen = new LoadingScreen(this);
        preferences = new AppPreferences();
        setScreen(loadingScreen);

        // tells our asset manger that we want to load the images set in
        // loadImages
        // method
        assetManager.queueAddMusic();
        // tells the asset manager to load the images and wait until finished
        // loading.
        assetManager.manager.finishLoading();
        // loads the 2 sounds we use
        playingSong = assetManager.manager.get("music/Rolemusic_-_pl4y1ng.mp3");
        if (preferences.isMusicEnabled()) {
            playingSong.play();
        }

    }

    /**
     * Change screen and set the logging level
     *
     * @param screen the screen
     */
    public void changeScreen(Screens screen) {
        switch (screen) {
            case MENU:
                if (menuScreen == null) {
                    menuScreen = new MenuScreen(this);
                }
                setScreen(menuScreen);
                break;
            case PREFERENCES:
                if (preferencesScreen == null) {
                    preferencesScreen = new PreferencesScreen(this);
                }
                setScreen(preferencesScreen);
                break;
            case APPLICATION:
                // always make new game screen so game can't start midway
                if (mainScreen == null) {
                    mainScreen = new GameScreen(this);
                } else {
                    mainScreen.resetWorld();
                }

                setScreen(mainScreen);
                break;
            case ENDGAME:
                if (endScreen == null) {
                    endScreen = new EndScreen(this);
                }
                setScreen(endScreen);
                break;
        }
        final boolean isDebug = preferences.isDebugEnabled();
        if (isDebug) {
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        } else {
            Gdx.app.setLogLevel(Application.LOG_ERROR);
        }
    }

    /**
     * @return the AppPreferences preferences
     */
    public AppPreferences getPreferences() {
        return preferences;
    }

    @Override
    public void dispose() {
        playingSong.dispose();
        assetManager.manager.dispose();
    }
}